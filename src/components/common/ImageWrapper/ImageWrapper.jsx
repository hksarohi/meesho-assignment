/**
 * Created by Harsh on 24/02/19.
 */
import React, { useEffect, useRef } from 'react';
import './ImageWrapper.less';

const ImageWrapper = (props) => {
  const ironImageHd = useRef(null);
  const preloadImage = useRef(null);


  useEffect(() => {
    const hdLoaderImg = new Image();
    const { srcLoaded } = props;
    hdLoaderImg.src = srcLoaded;


    hdLoaderImg.onload = () => {
      if (ironImageHd.current && preloadImage.current) {
        ironImageHd.current.setAttribute(
          'src',
          `${srcLoaded}`,
        );
        ironImageHd.current.classList.add('image-fade-in');
        preloadImage.current.classList.add('hide-preload');
      }
    };
    // action here
  }, [props]);


  return (
    <div className="image-container">

      <img
        alt="Image"
        className="image-loaded"
        ref={ironImageHd}
      />
      <img
        alt="Placeholder Image"
        className="image-preload"
        src={props.srcPreload}
        ref={preloadImage}
      />

    </div>
  );
};

export default ImageWrapper;

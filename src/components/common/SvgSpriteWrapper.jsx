import React, { Fragment } from 'react';

const SvgSpriteWrapper = ({
  className,viewBox, url, width, height, fill, isHidden,
}) => {
  if (typeof (window) === 'undefined' || isHidden) {
    return null;
  }
  if (width && height) {
    const obj = `<object type="image/svg+xml" fill=${fill} height=${height} width=${width} data=${url} ></object>`;
    return <span dangerouslySetInnerHTML={{ __html: obj }} />;
  }
  return <svg className={className} viewBox={viewBox}><use xlinkHref={url} /></svg>;
};

export default SvgSpriteWrapper;

import React from 'react';
import { connect } from 'react-redux';
import './NavigationHeader.less';
import Logo from '../../svgs/Logo.svg';
import LogoText from '../../svgs/LogoText.svg'
import SvgSpriteWrapper from '../common/SvgSpriteWrapper'

class NavigationBar extends React.Component {
  constructor(props) {
    super(props);
  }


  render() {
    return (
      <div>
        <a aria-label="Home" href="/" className="link">
          <SvgSpriteWrapper viewBox={Logo.viewBox} url={Logo.url} className="icon"/>
          <SvgSpriteWrapper viewBox={LogoText.viewBox} url={LogoText.url} className="text"/>

        </a>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
  };
}

export default connect(
  mapStateToProps,
  {  },
)(NavigationBar);

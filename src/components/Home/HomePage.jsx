import React from 'react';

import { connect } from 'react-redux';
import {
  redditRequest,
  redditResetStore,

} from '../../actions/HomeAction';

import {
  getObjectValueIfEmpty,
} from '../../utils/common';

import ListingHOC from '../common/ListingHOC/ListingHOC';
import FilterHOC from '../common/FilterHOC/FilterHOC';
import NotFound from '../NotFound/NotFound';


import RedditCard from './RedditCard';
import FilterCard from './FilterCard';

const RedditListing = ListingHOC(RedditCard);
const RedditFilters = FilterHOC(FilterCard);


class HomePage extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      filters: {
        alternativeart: false,
        pics: false,
        gifs: false,
        adviceanimals: false,
        cats: false,
        images: false,
        photoshopbattles: false,
        hmmm: false,
        all: false,
        aww: false,


      },
    };
    // this.props.loginResetStore();
  }


  getData=(subreddit) => {
    const { redditRequest } = this.props;

    const { filters } = this.state;

    Object.keys(filters).forEach((v) => { filters[v] = false; });

    filters[subreddit] = true;

    this.setState({ filters });

    redditRequest(subreddit);
  }

  componentDidMount() {
    this.getData('alternativeart');
  }

  filterReddits=(redditData) => {
    const imageReddits = [];
    for (let i = 0; i < redditData.length; i++) {
      if (redditData[i].data && redditData[i].data.post_hint === 'image') imageReddits.push(redditData[i].data);
    }
    return imageReddits;
  }

  getContent=(redditData, imageReddits) => {
    const { isLoading, isLoaded } = this.props;
    let listing = null;

    if ((isLoaded && !isLoading) && redditData.length < 1) {
      listing = (<NotFound content="Network Error!" />);
    } else if ((isLoaded && !isLoading) && imageReddits.length < 1) {
      listing = (<NotFound content="No Posts With images!" />);
    } else {
      listing = (<RedditListing listData={imageReddits} {...this.props} />);
    }
    return listing;
  }


  render() {
    let { redditData } = this.props;
    const { filters } = this.state;
    redditData = getObjectValueIfEmpty(redditData, 'children', []);
    const imageReddits = this.filterReddits(redditData);
    const listing = this.getContent(redditData, imageReddits);

    return (
      <div className="login-wrapper">
        <RedditFilters filters={filters} getData={this.getData} {...this.props} />
        {listing}
      </div>
    );
  }
}


function mapStateToProps(state) {
  const { HomeReducer } = state;
  return {
    redditData: HomeReducer.redditData,
    isLoading: HomeReducer.isLoading,
    isLoaded: HomeReducer.isLoaded,
  };
}

export default connect(
  mapStateToProps,
  {
    redditRequest,
    redditResetStore,

  },
)(HomePage);

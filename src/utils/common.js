
export function keyExists(key, obj) {
  if (
    (Array.isArray(obj) && key in obj)
    || (obj instanceof Object && Object.prototype.hasOwnProperty.call(obj, key))
  ) {
    return true;
  }

  return false;
}

// import logger from 'logger';
// const log = logger.createLogger(config.LOG_FILE_PATH);


export function isEmpty(obj) {
  let isempty = false;
  const type = typeof obj;

  isempty = isempty || !obj;
  isempty = isempty || type === 'undefined'; // if it is undefined
  isempty = isempty || obj === null; // if it is null
  isempty = isempty || (type === 'string' && obj === ''); // if the string is empty
  isempty = isempty || (obj === false || obj === 0); // if boolean value returns false
  isempty = isempty || (Array.isArray(obj) && obj.length === 0); // if array is empty
  isempty = isempty || (type === 'object' && Object.keys(obj).length === 0); // if object is empty

  return isempty;
}


// check if the strinc contains alphabets (spaces) only
export function isAlphabet(value, allowSpaces = true) {
  if (allowSpaces) {
    return value.match(/^[a-zA-Z\s]+$/);
  }

  return value.match(/^[a-zA-Z]+$/);
}

// check if the string contains pattern(bookingid) only
export function isIdPattern(value) {
  return value.match(/^[a-zA-Z0-9]+$/);
}

export function isEnumerable(obj) {
  let enumerable = false;

  if (Array.isArray(obj) || obj instanceof Object) {
    enumerable = true;
  }

  return enumerable;
}

export function getObjectValue(obj, key, defaultValue = null) {
  let enumerator = obj;
  let property = key;

  if (isEnumerable(enumerator) && keyExists(property, enumerator)) {
    return enumerator[property];
  }

  const dotLastIndex = property.lastIndexOf('.');

  if (dotLastIndex >= 0) {
    const withoutLastKey = property.substr(0, dotLastIndex);
    enumerator = getObjectValue(enumerator, withoutLastKey, defaultValue);
    property = property.substr(dotLastIndex + 1);
  }

  if (isEnumerable(enumerator)) {
    return keyExists(property, enumerator)
      ? enumerator[property]
      : defaultValue;
  }
  return defaultValue;
}

export const httpStatusCodes = {
  UNAUTHORIZED: 401,
  BAD_REQUEST: 400,
};

export function getObjectValueIfEmpty(obj, key, defaultValue = null) {
  const value = getObjectValue(obj, key);

  if (isEmpty(value)) {
    return defaultValue;
  }
  return value;
}

export function clone(oldObject, newObject) {
  return { ...oldObject, ...newObject };
}

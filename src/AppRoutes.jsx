import React, { lazy, Suspense } from 'react';
import {
  Switch, Route, withRouter,
} from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Loader from './components/Loader/Loader';


const HomeComponent = lazy(() => import('./components/Home/HomePage' /* webpackChunkName:"homePage" */));

const HomePage=()=> {
  return (
    <Suspense fallback={<Loader/>}>
      <HomeComponent />
    </Suspense>
  );
}



const Routes = props => (
  <Switch>
    <Route exact path="/" component={HomePage} title="Home" />
    <Route  component={HomePage} title="Home" />

  </Switch>
);


Routes.contextTypes = {
  router: PropTypes.object,
};
function mapStateToProps(state) {
  return {
    auth: state.loginUser,
  };
}

export default withRouter(connect(mapStateToProps)(Routes));

const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');

process.traceDeprecation = true;


module.exports = {
  entry: [
    './src/index.jsx',
  ],
  output: {
    path: path.resolve(__dirname, 'public/build'),
    filename: '[name].[hash].min.js',
    chunkFilename: '[name].[hash].js',

    publicPath: '/',
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  module: {
      rules: [
          {
              test: /\.(js|jsx)$/,
              exclude: /node_modules/,

              use: [{ loader: 'babel-loader' },
              ],
          },
          {
              test: /\.(svg)$/,

              use: [{ loader: 'svg-sprite-loader', options: { extract: true, spriteFilename: '[chunkname]-sprite.svg',esModule:false  }},{loader:'svgo-loader'}


              ],
          },
          {
              test: /\.less$/,
              use: [{ loader: 'style-loader' },
                  { loader: 'css-loader' },
                  { loader: 'less-loader' },
              ],
          },
          {
              test: /\.css$/,
              use: [{ loader: 'style-loader' },
                  { loader: 'css-loader' },
                  { loader: 'less-loader' },
              ],
          },
      ],

  },
  optimization: {
    runtimeChunk: 'single',
      splitChunks: {
          chunks: 'all',
          maxInitialRequests: Infinity,
          minSize: 0,

      },
  },
  plugins: [
      new SpriteLoaderPlugin(),

      new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }),

    new webpack.optimize.OccurrenceOrderPlugin(),
    // new WebpackS3Plugin({
    //     include: /.*\.js/,
    //     s3Options: {
    //         region: 'us-east-1',
    //         accessKeyId: 'AKIAJVXHKBV5HDGWFBWA',
    //         secretAccessKey: '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',
    //     },
    //     s3UploadOptions: {
    //         Bucket: 'casa2inns.assets',
    //     },
    //     basePath: 'coupon/assets/js',
    // }),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'public', 'index.html'),
      inject: 'body',
    }),
  ],

};
